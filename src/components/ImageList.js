import React, { Component } from 'react'
import { Row } from 'antd';
import GetImage from './GetImages';

class ImageList extends Component {
    render() {
        return (
            <Row type="flex" justify="start">
                <GetImage image={this.props.image}/>
            </Row>
        )
    }
}

export default ImageList