import React, { Component } from 'react'
import { Col } from 'antd';

class GetImage extends Component {
    render() {
        return (
            this.props.image.map((img) => {
                return(
                    <Col span={4} key={img.id}>
                        <div className="gallery" >
                            <a target="_blank" rel="noopener noreferrer" href={img.download_url}>
                                <img src={img.download_url} alt={img.author} width="200" height="200" />
                            </a>
                            <div className="desc">{img.author}</div>
                        </div>
                    </Col>
                )
            })
        )
    }
}

export default GetImage