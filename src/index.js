import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import ImageList from './components/ImageList'
import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js.map'
import './index.css'
import 'antd/dist/antd.css';
import ImageJson from './pictures.json'
import { Row, Col } from 'antd'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            render:'',
            images: [],
        }
    }

    getNature = (compName, e) => {
        this.setState(
            {
                render: compName,
                images: ImageJson[compName],
            }
        );
    }

    _getPhotos(){
        return <ImageList image={this.state.images}/>
    }

    render() {
        return (
            <div>
                <h2 className="text-center">Image Listing Demo</h2>
                <Row type="flex" justify="center">
                    <Col span={4}>
                        <button className="btn btn-primary" onClick={this.getNature.bind(this, 'nature')}>Nature</button>
                    </Col>
                    <Col span={4}>
                        <button className="btn btn-success" onClick={this.getNature.bind(this, 'food')}>Food</button>
                    </Col>
                    <Col span={4}>
                        <button className="btn btn-danger" onClick={this.getNature.bind(this, 'computer')}>Computer</button>
                    </Col>
                    {this._getPhotos()}
                </Row>
            </div>
        )
    }
}

ReactDOM.render(<Home/>, document.getElementById('container'));      
